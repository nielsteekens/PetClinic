#!/bin/sh
MyUSER="$1"
MyPASS="$2"

mv /etc/mysql/my.cnf my.cnf.in
sed -e s/127.0.0.1/0.0.0.0/g my.cnf.in | tee /etc/mysql/my.cnf

service mysql restart

C1="CREATE DATABASE IF NOT EXISTS petclinic;"
C2="USE petclinic;"
C3="GRANT ALL PRIVILEGES ON *.*  TO '$MyUSER'@'%' IDENTIFIED BY '$MyPASS' WITH GRANT OPTION;"
C4="FLUSH PRIVILEGES;"
SQL="${C1}${C2}${C3}${C4}"

mysql -h localhost --user="$MyUSER" --password="$MyPASS" -Bse "$SQL"

exit 0


